#include "Algorithm.h"

class Prima : public Algorithm {
public:
	int root = 0;

	std::vector<std::pair<int, int>> get_edge_list() override {
		return edge_list;
	}

	void set_root(int val) override {
		root = val;
	}

	void execute(std::vector<std::vector<float>> connections) override {
		int N = connections.size();
		for (int i = 0;i < N;i++) {
			visited.push_back(false);
		}

		std::vector<int> vertex_subset = { root };
		visited[root] = true;

		while (vertex_subset.size() != N) {
			float min_distance = 1e9;
			int new_vertex = -1;
			std::pair<int, int> edge;
			for (auto vertex : vertex_subset) {
				for (int i = 0;i < N;i++) {
					if (connections[vertex][i] != 0 && !visited[i]) {
						if (connections[vertex][i] < min_distance ){
							min_distance = connections[vertex][i];
							edge = std::make_pair(vertex, i);
							new_vertex = i;
						}
					}
				}
			}
			vertex_subset.push_back(new_vertex);
			edge_list.push_back(edge);
			visited[new_vertex] = true;
		}
	}

	void set_destination(int dest) {}
	void set_metrics(std::vector<float>) {};

private:
	std::vector<std::pair<int, int>> edge_list;
	std::vector<bool> visited;
};
