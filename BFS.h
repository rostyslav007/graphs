#include "Algorithm.h"
#include <queue>

class BFS : public Algorithm {
public:
	int root = 0;

	void set_root(int val) override {
		root = val;
	}

	void set_destination(int dest){}
	void set_metrics(std::vector<float>) {};

	std::vector<std::pair<int, int>> get_edge_list() override {
		return edge_list;
	}

	void execute(std::vector<std::vector<float>> connections) override {


		std::queue<int> vertex_order;
		vertex_order.push(root);
		int N = connections.size();

		for (int i = 0;i < N;i++) {
			visited.push_back(false);
		}
		visited[root] = true;

		while (!vertex_order.empty()) {
			int current_vertex = vertex_order.front();
			vertex_order.pop();

			for (int i = 0;i < N;i++) {
				if (!visited[i] && connections[current_vertex][i] != 0) {
					edge_list.push_back(std::make_pair(current_vertex, i));
					vertex_order.push(i);
					visited[i] = true;
				}
			}
		}
	}

private:
	std::vector<std::pair<int, int>> edge_list;
	std::vector<bool> visited;
};