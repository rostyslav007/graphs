#include "Algorithm.h"

class Dijkstra : public Algorithm {
public:
	int root = 0;

	std::vector<std::pair<int, int>> get_edge_list() override {
		return edge_list;
	}

	void set_root(int val) override {
		root = val;
	}
	
	void set_destination(int to) override {
		dest = to;
	}

	void execute(std::vector<std::vector<float>> connections) override {
		float inf = 1e9;
		int N = connections.size();

		if (dest == -1) {
			dest = N - 1;
		}

		for (int i = 0;i < N;i++) {
			from_index.push_back(-1);
			visited.push_back(false);
			distances.push_back(inf);
		}

		from_index[root] = root;
		distances[root] = 0;

		int unvisited_count = N;
		while (!visited[dest]) {
			int current_vertex = -1;
			float min_distance = inf;
			for (int i = 0;i < N;i++) {
				if (!visited[i] && distances[i] < min_distance) {
					min_distance = distances[i];
					current_vertex = i;
				}
			}
			visited[current_vertex] = true;

			for (int v = 0;v < N;v++) {
				if ((!visited[v]) && connections[current_vertex][v] != 0 && (distances[current_vertex] + connections[current_vertex][v] < distances[v])) {
					distances[v] = distances[current_vertex] + connections[current_vertex][v];
					from_index[v] = current_vertex;
				}
			}
		}

		std::cout << distances[dest] << std::endl;
		int vertex = dest;
		while (vertex != root) {
			edge_list.insert(edge_list.begin() ,std::make_pair(from_index[vertex], vertex));
			vertex = from_index[vertex];
		}
	}

	void set_metrics(std::vector<float>) {};
private:
	int dest = -1;
	std::vector<int> from_index;
	std::vector<float> distances;
	std::vector<std::pair<int, int>> edge_list;
	std::vector<bool> visited;
};