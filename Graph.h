#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "Algorithm.h"

class Graph {
public:

	Graph() = default;

	Graph(int N) : N(N) {
		for (int i = 0;i < N;i++) {
			std::vector<float> row;
			for (int j = 0;j < N;j++) {
				row.push_back(0);
			}
			matrix.push_back(row);
		}
	};

	int edges_count() const {
		int count = 0;
		for (int i = 0;i < N;i++) {
			for (int j = 0;j < N;j++) {
				if (matrix[i][j] != 0) {
					count++;
				}
			}
		}
		return directed ? (count) : (count / 2);
	}

	void set_directed(bool orient){
		directed = orient;
	}

	void load_from_file(std::string filepath);
	void show_graph();
	void set_strategy(Algorithm* strategy) {
		delete this->strategy;
		this->strategy = strategy;
	}
	void execute() {
		this->strategy->execute(matrix);
	}
	std::vector<std::pair<int, int>> get_edge_list();
	void set_root(int r) {
		strategy->set_root(r);
	}


private:
	Algorithm *strategy = nullptr;
	std::vector<std::vector<float>> matrix;
	bool directed = false;
	int N;
};

void Graph::load_from_file(std::string filepath) {

	std::string row;
	std::ifstream file(filepath);

	while (std::getline(file, row)) {
		int space_pos = row.find(" ");
		int from = std::stoi(row.substr(0, space_pos));

		int space_pos_2 = row.find(" ", space_pos + 1);
		int to = std::stoi(row.substr(space_pos + 1, space_pos_2 - space_pos));
		float weight = 1.0;
		if (space_pos_2 != std::string::npos) {
			weight = std::stof(row.substr(space_pos_2 + 1, row.length() - space_pos_2));
		}

		matrix[from][to] = weight;
		if (!directed) {
			matrix[to][from] = weight;
		}
	}
}


std::vector<std::pair<int, int>> Graph::get_edge_list() {
	return this->strategy->get_edge_list();
}


void Graph::show_graph() {
	for (int i = 0;i < N;i++) {
		for (int j = 0;j < N;j++) {
			std::cout << matrix[i][j] << " ";
		}
		std::cout << std::endl;
	}
}