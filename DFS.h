#include "Algorithm.h"


class DFS : public Algorithm {
public:
	int root = 0;
	
	void set_root(int r) {
		root = r;
	}

	void set_destination(int dest) {}
	void set_metrics(std::vector<float>) {};

	std::vector<std::pair<int, int>> get_edge_list() override{
		return edge_list;
	}

	void execute(std::vector<std::vector<float>> connections) override {
		N = connections.size();
		for (int i = 0;i < N;i++) {
			visited.push_back(false);
		}
		dfs(connections, root, visited);
	}
private:

	void dfs(std::vector<std::vector<float>> &connections, int current_vertex, std::vector<bool> &visited) {
		visited[current_vertex] = true;

		for (int i = 0;i < N; i++) {
			if (connections[current_vertex][i] != 0 && !visited[i]) {
				edge_list.push_back(std::make_pair(current_vertex, i));
				dfs(connections, i, visited);
			}
		}
	}
	int N;
	std::vector<std::pair<int, int>> edge_list;
	std::vector<bool> visited;
};