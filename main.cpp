#include <iostream>
#include "Graph.h"
#include "BFS.h"
#include "DFS.h"
#include "Prima.h"
#include "Dijkstra.h"
#include "AStar.h"
#include "FordFulkerson.h"
#include "SocialNetwork.h"

using std::cout;

int main()
{
	std::string filepath = "in_graph.txt";
	std::string social_net_path = "Social Network Data/facebook_combined.txt";
	/*Graph graph(6);
	graph.load_from_file(filepath);
	graph.show_graph();

	Algorithm* star = new FordFulkerson();
	
	star->set_destination(5);
	
	graph.set_strategy(star);

	graph.execute();
	auto res = graph.get_edge_list();
	for (auto p : res) {
		std::cout << p.first << " -> " << p.second << std::endl;
	}*/

	SocialNetwork facebook_users_net;
	facebook_users_net.load_from_file(social_net_path);
	cout << facebook_users_net.edges_count();
}

