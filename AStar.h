#include "Algorithm.h"
#include <deque>
#include <iostream>

class AStar : public Algorithm {
public:

	int root = 0;

	void set_root(int val) override {
		root = val;
	}

	std::vector<std::pair<int, int>> get_edge_list() {
		return edge_list;
	}

	void set_metrics(std::vector<float> metrics) {
		this->metrics = metrics;
	}

	void set_destination(int dest) override{
		this->dest = dest;
	}

	/*void _bin_search_element(std::deque<std::pair<int, float>> deq) {
		int left = 0;
		int right = deq.size();
	}*/

	void execute(std::vector<std::vector<float>> connections) override {
		float inf = 1e9;
		int N = connections.size();

		if (dest == -1) {
			dest = N - 1;
		}

		for (int i = 0;i < N;i++) {
			from_index.push_back(-1);
			visited.push_back(false);
			astar_cost.push_back(0);
			distances.push_back(inf);
			if (metrics.size() != N) {
				metrics.push_back(0);
			}
		}

		std::deque<std::pair<int, float>> node_hauristic_sorted;

		from_index[root] = root;
		distances[root] = 0;
		astar_cost[root] = metrics[root];
		node_hauristic_sorted.push_back(std::make_pair(root, astar_cost[root]));

		while (!visited[dest]) {
			float min_distance = inf;
			int current_vertex = node_hauristic_sorted.front().first;
			node_hauristic_sorted.pop_front();

			for (int i = 0;i < N;i++) {
				if (!visited[i] && connections[current_vertex][i]) {
					int new_ver = i;
					float new_hauristic = distances[current_vertex] + connections[current_vertex][i] + metrics[i];
					node_hauristic_sorted.push_back(std::make_pair(new_ver, new_hauristic));
					astar_cost[new_ver] = new_hauristic;

					// Bin search check if vertex already in deque

					std::sort(node_hauristic_sorted.begin(), node_hauristic_sorted.end(), 
						[](const std::pair<int, float>& x, const std::pair<int, float>& y)
						{
							return x.second < y.second;
						});
				}
			}
			visited[current_vertex] = true;
			for (int v = 0;v < N;v++) {
				if ((!visited[v]) && connections[current_vertex][v] != 0 && (distances[current_vertex] + connections[current_vertex][v] < distances[v])) {
					distances[v] = distances[current_vertex] + connections[current_vertex][v];
					from_index[v] = current_vertex;
				}
			}
		}

		int vertex = dest;
		while (vertex != root) {
			edge_list.insert(edge_list.begin(), std::make_pair(from_index[vertex], vertex));
			vertex = from_index[vertex];
		}
	}

private:
	int dest = -1;
	std::vector<float> astar_cost;
	std::vector<bool> visited;
	std::vector<int> from_index;
	std::vector<float> distances;
	std::vector<float> metrics;
	std::vector<std::pair<int, int>> edge_list;
};