#include "Algorithm.h"
#include <deque>
#include <iostream>

class FordFulkerson : public Algorithm {
public:
	int root = 0;

	std::vector<std::pair<int, int>> get_edge_list() {
		return edge_list;
	}

	void set_root(int val) {
		root = val;
	}

	void set_destination(int to) {
		dest = to;
	}
	
	void set_metrics(std::vector<float> metrics) {}

	void execute(std::vector<std::vector<float>> connections) {
		N = connections.size();
		if (dest == -1) {
			dest = N - 1;
		}
		flow_capacity = connections;
		std::vector<bool> visited(N, false);
		float f = 0;
		while (find_path(-1, root, visited, flow_capacity)) {
			float min_flow = 1e9;
			for (auto fl : max_flows) {
				if (fl < min_flow) {
					min_flow = fl;
				}
			}
			f += min_flow;
			std::cout << min_flow << " ";

			for (auto p : edge_list) {
				if (p.first == -1) {
					continue;
				}
				int i = p.first, j = p.second;
				flow_capacity[i][j] -= min_flow;
				flow_capacity[j][i] += min_flow;
			}

			max_flows.clear();
			edge_list.clear();
			visited = std::vector<bool>(N, false);
		}
		std::cout << "\n";
		for (int i = 0;i < N;i++) {
			for (int j = 0;j < N;j++) {
				std::cout << flow_capacity[i][j] << " ";
				if (i < j && connections[i][j] > 0) {
					edge_flow_capacity.push_back(std::make_pair(std::make_pair(i, j), flow_capacity[j][i]));
				}
			}
			std::cout << std::endl;
		}
		std::cout << f;
		save_flow();
	}

	void save_flow(std::string file = "edge_list.txt") {
		std::ofstream out;
		out.open(file);
		for (auto p : edge_flow_capacity) {
			out << p.first.first << " " << p.first.second << " " << p.second << "\n";
		}
		out.close();
	}

private:

	bool find_path(int from, int current_vertex, std::vector<bool> &visited, std::vector<std::vector<float>> capacity) {
		visited[current_vertex] = true;

		if (current_vertex == dest) {
			edge_list.push_back(std::make_pair(from, current_vertex));
			return true;
		}

		std::deque<std::pair<int, float>> vertex_capacities;

		for (int i = 0;i < N;i++) {
			if (!visited[i] && capacity[current_vertex][i] > 0) {
				vertex_capacities.push_back(std::make_pair(i, capacity[current_vertex][i]));
			}
		}

		std::sort(vertex_capacities.begin(), vertex_capacities.end(), [](const std::pair<int, float>& x, const std::pair<int, float>& y)
			{
				return x.second > y.second;
			});

		auto deque_it = vertex_capacities.begin();
		while (deque_it != vertex_capacities.end()) {
			std::pair<int, float> v_c_pair = *deque_it;
			int vertex = v_c_pair.first;
			float flow = v_c_pair.second;

			bool status = find_path(current_vertex, vertex, visited, capacity);
			if (status) {
				max_flows.push_back(flow);
				edge_list.push_back(std::make_pair(from, current_vertex));
				return true;
			}
			visited[vertex] = false;
			deque_it++;
		}

		return false;

	}

	int N;
	int dest = -1;
	std::vector<std::vector<float>> flow_capacity;
	std::vector<float> max_flows;
	std::vector<std::pair<int, int>> edge_list;
	std::vector<std::pair<std::pair<int, int>, float>> edge_flow_capacity;
};
