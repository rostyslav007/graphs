#include "Graph.h"
#include <unordered_map>
#include <unordered_set>
#include "BidirectDeijkstra.h"
#include <fstream>

class SocialNetwork {
public:
	SocialNetwork() = default;

	int edges_count() {
		int c = 0;
		int max_f = 0;
		for (auto p : index_user_map) {
			int d = p.second.size();
			c += d;
			if (d > max_f) {
				max_f = d;
			}
		}
		//std::cout << max_f;
		return c / 2;
	}

	int get_distance(int v1, int v2) {
		if (indexes.find(v1) == indexes.end() || indexes.find(v2) == indexes.end()) {
			return -1;
		}

		int dist = bd.execute(index_user_map, indexes, v1, v2);

		return dist;
	}

	std::vector<std::pair<int, int>> get_edge_list() const {
		return bd.get_edge_list();
	}

	void load_from_file(std::string filepath) {
		std::ifstream file(filepath);
		std::string row;
		while (getline(file, row)) {
			int space_pos = row.find(" ");
			int from = std::stoi(row.substr(0, space_pos));
			int to = std::stoi(row.substr(space_pos + 1, row.length() - space_pos));
			if (indexes.find(from) != indexes.end()) {
				index_user_map[from].push_back(User {to, "" });
			}
			else {
				max_index = std::max(from, max_index);
				indexes.insert(from);
				index_user_map[from] = std::vector<User>{ User {to, ""}};
			}

			if (indexes.find(to) != indexes.end()) {
				index_user_map[to].push_back(User{ from, "" });
			}
			else {
				max_index = std::max(to, max_index);
				indexes.insert(to);
				index_user_map[to] = std::vector<User>{ User {from, ""} };
			}
		}
	}

private:
	BidirectionalDeijkstra bd;
	int max_index;
	std::unordered_map<int, std::vector<User>> index_user_map;
	std::unordered_set<int> indexes;
};
