#include <map>
struct User {
	int idx;
	std::string information = "";
};

class BidirectionalDeijkstra {
public:
	BidirectionalDeijkstra() = default;

	std::vector<std::pair<int, int>> get_edge_list() const {
		return edge_list;
	}

	int execute(std::unordered_map<int, std::vector<User>> index_user_map, std::unordered_set<int> indexes, int v1, int v2) {
		int m = -1;
		for (auto v : indexes) {
			if (v > m) {
				m = v;
			}
		}
		float inf = 1e9;

		std::vector<bool> visited_from_v1(m, false);
		std::vector<bool> visited_from_v2(m, false);
		std::vector<float> distance_from_v1(m, inf);
		std::vector<float> distance_from_v2(m, inf);
		std::vector<int> from_v1(m, -1);
		std::vector<int> from_v2(m, -1);

		distance_from_v1[v1] = 0;
		distance_from_v2[v2] = 0;

		float p = inf, e = 1;
		int common_vertex = -1;
		bool out = false;
		while(!out) {
			out = true;
			int v1_next = -1, v2_next = -1;
			float min_v1 = inf;
			float min_v2 = inf;
			for (int i = 0;i < m;i++) {
				if (!visited_from_v1[i] && distance_from_v1[i] < min_v1) {
					min_v1 = distance_from_v1[i];
					v1_next = i;
				}
				if (!visited_from_v2[i] && distance_from_v2[i] < min_v2) {
					min_v2 = distance_from_v2[i];
					v2_next = i;
				}
			}
			visited_from_v1[v1_next] = true;
			visited_from_v2[v2_next] = true;
			
			if (min_v1 + min_v2 + e >= p) {

				int vertex = common_vertex;
				while (vertex != v2) {
					edge_list.insert(edge_list.begin(), std::make_pair(vertex, from_v2[vertex]));
					vertex = from_v2[vertex];
				}
				std::reverse(edge_list.begin(), edge_list.end());
				vertex = common_vertex;
				while (vertex != v1) {
					edge_list.insert(edge_list.begin(), std::make_pair(from_v1[vertex], vertex));
					vertex = from_v1[vertex];
				}

				return p;
			}
			if (v1_next != -1) {
				out = false;
				for (auto u : index_user_map[v1_next]) {
					auto succ = u.idx;
					if (!visited_from_v1[succ] && distance_from_v1[v1_next] + 1 < distance_from_v1[succ]) {
						distance_from_v1[succ] = distance_from_v1[v1_next] + 1;
						from_v1[succ] = v1_next;
						if (visited_from_v2[succ] && distance_from_v1[succ] + distance_from_v2[succ] < p) {
							p = distance_from_v1[succ] + distance_from_v2[succ];
							common_vertex = succ;
						}
					}
				}
			}

			if (v2_next != -1) {
				out = false;
				for (auto u : index_user_map[v2_next]) {
					auto succ = u.idx;
					if (!visited_from_v2[succ] && distance_from_v2[v2_next] + 1 < distance_from_v2[succ]) {
						distance_from_v2[succ] = distance_from_v2[v2_next] + 1;
						from_v2[succ] = v2_next;
						if (visited_from_v1[succ] && distance_from_v1[succ] + distance_from_v2[succ] < p) {
							p = distance_from_v1[succ] + distance_from_v2[succ];
							common_vertex = succ;
						}
					}
				}
			}
		}
		return -1;
	}
private:
	std::vector<std::pair<int, int>> edge_list;
};