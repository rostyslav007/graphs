#pragma once
#include <vector>
#include <algorithm>

class Algorithm {
public:
	virtual void execute(std::vector<std::vector<float>> connections) = 0;
	virtual std::vector<std::pair<int, int>> get_edge_list() = 0;
	virtual void set_root(int val) = 0;
	virtual void set_destination(int to) = 0;
	virtual void set_metrics(std::vector<float> metrics) = 0;
private:
	std::vector<std::pair<int, int>> edge_list;
};
